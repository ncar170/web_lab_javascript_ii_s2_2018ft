
var imageCollection = [
    {"name": "arctic_fox: ", "source": "arctic_fox", "description": "Picture of an Arctic Fox"},
    {"name": "grazing_wombat", "source": "grazing_wombat", "description": "Picture of a grazing Wombat"},
    {"name": "Himalayan_Pika", "source": "Himalayan_Pika", "description": "Picture of a Himalayan Pika"},
    {"name": "lynx", "source": "lynx", "description": "Picture of a Lynx"},
    {"name": "Pallas_Cat","source": "Pallas_Cat", "description": "Picture of a Pallas Cat"},
    {"name": "Pallas_Cat2","source": "Pallas_Cat2", "description": "Picture of a Pallas Cat"},
    {"name": "pika", "source": "pika", "description": "Picture of a Pika"},
    {"name": "quokka","source": "quokka", "description": "Picture of a Quokka"},
    {"name": "RedPanda","source": "RedPanda", "description": "Picture of a Red Panda"},
    {"name": "wombat2","source": "wombat2", "description": "Picture of a Wombat"},
    {"name": "wombats","source": "wombats", "description": "Picture of a Wombat"}
];

var selectedImage = 0; // index of image selected, between 0 and length of array non-inclusive

function turnPage() {
    var newPage = document.getElementsByClassName("pageAnimation");

    for (var i = 0; i < newPage.length; i++) {

        var page = newPage[i];
        page.src = "../images/quokka.jpg";
    }


}