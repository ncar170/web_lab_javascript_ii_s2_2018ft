var customers = [
	{ "name" : "Peter Jackson", "gender" : "male", "year_born" : 1961, "joined" : "1997", "num_hires" : 17000 },
		
	{ "name" : "Jane Campion", "gender" : "female", "year_born" : 1954, "joined" : "1980", "num_hires" : 30000 },
	
	{ "name" : "Roger Donaldson", "gender" : "male", "year_born" : 1945, "joined" : "1980", "num_hires" : 12000 },
	
	{ "name" : "Temuera Morrison", "gender" : "male", "year_born" : 1960, "joined" : "1995", "num_hires" : 15500 },
	
	{ "name" : "Russell Crowe", "gender" : "male", "year_born" : 1964, "joined" : "1990", "num_hires" : 10000 },
	
	{ "name" : "Lucy Lawless", "gender" : "female", "year_born" : 1968, "joined" : "1995", "num_hires" : 5000 },	
		
	{ "name" : "Michael Hurst", "gender" : "male", "year_born" : 1957, "joined" : "2000", "num_hires" : 15000 },
		
	{ "name" : "Andrew Niccol", "gender" : "male", "year_born" : 1964, "joined" : "1997", "num_hires" : 3500 },	
	
	{ "name" : "Kiri Te Kanawa", "gender" : "female", "year_born" : 1944, "joined" : "1997", "num_hires" : 500 },	
	
	{ "name" : "Lorde", "gender" : "female", "year_born" : 1996, "joined" : "2010", "num_hires" : 1000 },	
	
	{ "name" : "Scribe", "gender" : "male", "year_born" : 1979, "joined" : "2000", "num_hires" : 5000 },

	{ "name" : "Kimbra", "gender" : "female", "year_born" : 1990, "joined" : "2005", "num_hires" : 7000 },
	
	{ "name" : "Neil Finn", "gender" : "male", "year_born" : 1958, "joined" : "1985", "num_hires" : 6000 },	
	
	{ "name" : "Anika Moa", "gender" : "female", "year_born" : 1980, "joined" : "2000", "num_hires" : 700 },
	
	{ "name" : "Bic Runga", "gender" : "female", "year_born" : 1976, "joined" : "1995", "num_hires" : 5000 },
	
	{ "name" : "Ernest Rutherford", "gender" : "male", "year_born" : 1871, "joined" : "1930", "num_hires" : 4200 },
	
	{ "name" : "Kate Sheppard", "gender" : "female", "year_born" : 1847, "joined" : "1930", "num_hires" : 1000 },
	
	{ "name" : "Apirana Turupa Ngata", "gender" : "male", "year_born" : 1874, "joined" : "1920", "num_hires" : 3500 },
	
	{ "name" : "Edmund Hillary", "gender" : "male", "year_born" : 1919, "joined" : "1955", "num_hires" : 10000 },
	
	{ "name" : "Katherine Mansfield", "gender" : "female", "year_born" : 1888, "joined" : "1920", "num_hires" : 2000 },
	
	{ "name" : "Margaret Mahy", "gender" : "female", "year_born" : 1936, "joined" : "1985", "num_hires" : 5000 },
	
	{ "name" : "John Key", "gender" : "male", "year_born" : 1961, "joined" : "1990", "num_hires" : 20000 },
	
	{ "name" : "Sonny Bill Williams", "gender" : "male", "year_born" : 1985, "joined" : "1995", "num_hires" : 15000 },
	
	{ "name" : "Dan Carter", "gender" : "male", "year_born" : 1982, "joined" : "1990", "num_hires" : 20000 },
	
	{ "name" : "Bernice Mene", "gender" : "female", "year_born" : 1975, "joined" : "1990", "num_hires" : 30000 }	
];

// function capitaliseGender() {
//
// 	for (var i =0; i < customers.length; i++) {
// 		customers[i].gender.charAt(0).toUpperCase();
// 	}
//
// }

function makeTable() {

	var tableDiv = document.getElementById("tableContainer");
	var table = document.createElement("table");

	table.style.borderStyle = "solid";

	table.id = "videoTable";

	var thead = document.createElement("thead");
	var row = document.createElement("tr");
	var column1 = document.createElement("th");
	column1.innerHTML = "Customer name";
	var column2 = document.createElement("th");
	column2.innerHTML = "Gender";
	var column3 = document.createElement("th");
	column3.innerHTML = "Year of birth";
	var column4 = document.createElement("th");
	column4.innerHTML = "Year joined";
	var column5 = document.createElement("th");
	column5.innerHTML = "Number of hires";

	var tbody = document.createElement("tbody");

	row.appendChild(column1);
	row.appendChild(column2);
	row.appendChild(column3);
	row.appendChild(column4);
	row.appendChild(column5);

	thead.appendChild(row);

	table.appendChild(thead);
	table.appendChild(tbody);

	tableDiv.appendChild(table);

	for (var i = 0; i < customers.length; i++) {

		var row1 = document.createElement("tr");
		var cell1 = document.createElement("td");
		cell1.innerHTML = customers[i].name;
		var cell2 = document.createElement("td");
		cell2.innerHTML = customers[i].gender;
		var cell3 = document.createElement("td");
		cell3.innerHTML = customers[i].year_born;
		var cell4 = document.createElement("td");
		cell4.innerHTML =customers[i].joined;
		var cell5 = document.createElement("td");
		cell5.innerHTML = customers[i].num_hires;

		row1.appendChild(cell1);
		row1.appendChild(cell2);
		row1.appendChild(cell3);
		row1.appendChild(cell4);
		row1.appendChild(cell5);

		tbody.appendChild(row1);
		table.appendChild(row1);
	}

}

function makeStatistics() {

    males = 0; //set a variable that holds our total
	females = 0;
    for (i = 0; i < customers.length; i++) {
    	//loop through the array
        if (customers[i].gender.value = "male"){
            males++;  //add the value in 'jobs' to the total
		} else {
        	females++;
		}
    }

    age0 = 0;
    age31 = 0;
    age65 =0;

    for (i = 0; i < customers.length; i++){
		if (customers[i].year_born > 1988){
			age0 ++;
		} else if (customers[i].year_born >= 1954 && customers[i].year_born <= 1987){
			age31++
		} else {
			age65++;
		}
	}

	gold = 0;
    silver = 0;
    bronze = 0;

    for (i = 0; i < customers.length; i++){
    	if (customers[i].num_hires > 20000){
    		gold++;
		} else if (customers[i].num_hires >= 10000 && customers[i].num_hires <= 19999){
    		silver++;
		} else {
    		bronze++;
		}
	}

	var summaryDiv = document.getElementById("summaryStatistics");

	var summaryTable = document.createElement("table");

	summaryTable.style.borderStyle = "solid";

	summaryTable.id = "summaryStatisticsTable";

	var thead = document.createElement("thead");
	var row = document.createElement("tr");
	var column1 = document.createElement("th");
	column1.innerHTML = "Male";
	var column2 = document.createElement("th");
	column2.innerHTML = "Female";
	var column3 = document.createElement("th");
	column3.innerHTML = "0-30";
	var column4 = document.createElement("th");
	column4.innerHTML = "31-64";
	var column5 = document.createElement("th");
	column5.innerHTML = "65+";
	var column6 = document.createElement("th");
	column6.innerHTML = "Gold";
	var column7 = document.createElement("th");
	column7.innerHTML = "Silver";
	var column8 = document.createElement("th");
	column8.innerHTML = "Bronze";

	var tbody = document.createElement("tbody");

	row.appendChild(column1);
	row.appendChild(column2);
    row.appendChild(column3);
    row.appendChild(column4);
    row.appendChild(column5);
    row.appendChild(column6);
    row.appendChild(column7);
    row.appendChild(column8);

    thead.appendChild(row);

    summaryTable.appendChild(thead);
    summaryTable.appendChild(tbody);

    summaryDiv.appendChild(summaryTable);

    var row1 = document.createElement("tr");

    var cell1 = document.createElement("td");
    cell1.innerHTML = males;

    var cell2 = document.createElement("td");
    cell2.innerHTML = females;

    var cell3 = document.createElement("td");
    cell3.innerHTML = age0;
    var cell4 = document.createElement("td");
    cell4.innerHTML = age31;
    var cell5 = document.createElement("td");
    cell5.innerHTML = age65;

    var cell6 = document.createElement("td");
    cell6.innerHTML = age0;
    var cell7 = document.createElement("td");
    cell7.innerHTML = age31;
    var cell8 = document.createElement("td");
    cell8.innerHTML = age65;

    row1.appendChild(cell1);
    row1.appendChild(cell2);
    row1.appendChild(cell3);
    row1.appendChild(cell4);
    row1.appendChild(cell5);
    row1.appendChild(cell6);
    row1.appendChild(cell7);
    row1.appendChild(cell8);
    tbody.appendChild(row1);
    summaryTable.appendChild(row1);

}
